# Arch Mini Install
(on QEMU/KVM virtual machine BIOS mode)

This is my "almost" step by step guide for installing Arch Linux on QEMU/KVM virtual machine. Afther this you will get minimal working Arch Linux whitout GUI.

As a reference you can use [Arch Linux wiki](https://wiki.archlinux.org/index.php/Installation_guide).

## Pre-installation

### Acquire installation image from download page and verify signature.

```
$ gpg --keyserver-options auto-key-retrieve --verify archlinux-version-x86_64.iso.sig
```
### Prepare an installation media, USB flash drive, optical disc or network with PXE.

For QEMU/KVM virtual machine copy iso image to */var/lib/libvirt/images/*.

### Crate virtual machine

Create virtual machine from local install media. Choose *archlinux-version-x86_64.iso*, and for operating system Arch Linux. Choose CPU and memory settings. Create disk image for virtual machine. Custimize configuration before install, verify that firmware is set to BIOS.

### Boot the live enviroment

Begin installation. From boot menu choose first option. In few seconds you will be loged in as root user in archiso zsh shell.

### Set the keyboard layout

The default console keymap is set to US, if you need to change it list available layouts and set your choice.

```
# ls /usr/share/kbd/keymaps/**/*.map.gz
# loadkeys de-latin1
```

### Verify the boot mode

List the efivars directory.

```
# ls /sys/firmware/efi/efivars
```

If directory does not exist, it is ok, you are booted in BIOS mode.

### Connect to the internet

Check ip address and internet connection.

```
# ip address show
# ping -c 3 archlinux.org
```

### Update the system clock

Start network time synchronization.

```
# timedatectl set-ntp true
```

### Partition the disks

List disk drives, then create partitions with **cfdisk**.

```
# lsblk
```

or

```
# fdisk -l
# cfdisk /dev/vda
```

Create SWAP and root partition.

```
dos
/dev/vda1        4G    Linux swap
/dev/vda2    *  26G    Linux
```

### Format partitions

```
# mkswap /dev/vda1
# mkfs.ext4 /dev/vda2
```

### Mount the file systems

```
# mount /dev/vda2 /mnt
# swapon /dev/vda1
```

## Installation

### Select the mirrors

On live system **reflector** updates mirror list by choosing 70 most recently syncronized HTTPS mirrors and sorting them by download rate. You can check it in */etc/pacman.d/mirrorlist*.

### Install essential packages

```
# pacstrap /mnt base linux linux-firmware vim sudo man-db man-pages
```

Packages vim, sudo and man is not essential, I use them later in installation and configuration preces.

## Configure the system

### Fstab

Generate fstab for this installation.
```
# genfstab -U /mnt >> /mnt/etc/fstab
```

### Chroot

```
# arch-chroot /mnt /bin/bash
```

### Time zone

Set your local timezone.

```
# ln -sf /usr/share/zoneinfo/Europe/Belgrade /etc/localtime
# date
```

### Localization

Generate locals, edit */etc/locale.gen*.

```
# vim /etc/locale.gen
```

Find your local/locales and remove comment infront (#).

```
en_US.UTF-8
sr_RS@latin UTF-8
```

Save file and run **locale-gen**.

```
# locale-gen
# echo "LANG=en_US.UTF-8" > /etc/locale.conf
```

### Network configuration

Set hostname and edit */etc/hosts* file.

```
# echo myarch > /etc/hostname
# vim /etc/hosts
```

```
127.0.0.1		localhost
::1					localhost
127.0.1.1		myarch.localdomain   myarch
```

Install and enable NetworkManager.

```
# pacman -S networkmanager
# systemctl enable NetworkManager
```

### Initramfs (only for LVM, system encryption or RAID)

### Root password

Set root password.

```
# passwd
```

### Boot loader

Install and configure grub boot loader.

```
# pacman -S grub intel-ucode
# lsblk
# grub-install --target=i386-pc /dev/vda 
# grub-mkconfig -o /boot/grub/grub.cfg
```

## Reboot

Exit chroot enviroment, unmount all partitions and reboot.

```
# exit
# umount -R /mnt
# reboot
```

**Minimal Arch Linux is installed!!!**

You should have around 145 packages installed.

```
# pacman -Qq | wc -l
```

## Post-installation

Create user and install packages...

- https://wiki.archlinux.org/index.php/General_recommendations