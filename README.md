# Arch Mini Install

Install guide for ArchLinux.

In the end you will get minimal working ArchLinux without GUI.

#### Versions

- [QEMU/KVM BIOS](qemu-kvm-bios.md)

As a reference you can use [ArchLinux wiki](https://wiki.archlinux.org/index.php/Installation_guide).

## Pre-installation

### Acquire installation image from download page and verify signature

```
$ gpg --keyserver-options auto-key-retrieve --verify archlinux-version-x86_64.iso.sig
```
### Prepare an installation media, USB flash drive, optical disc or network with PXE

For USB flash drive:

```
$ sudo dd bs=4M if=archlinux-version-x86_64.iso of=/dev/sdX status=progress oflag=sync
```

### Boot the live enviroment

### Set the keyboard layout

### Verify the boot mode

### Connect to the internet

### Update the system clock

### Partition the disks

###### BIOS with MBR
| Mount point | Partition | Partition type | Suggested size |
| ----------- | --------- | -------------- | -------------- | 
| SWAP | /dev/swap_partition | Linux swap | more then 512M |
| /mnt | /dev/root_partition | Linux | Remainded of the device|


###### UEFI with GPT
| Mount point | Partition | Partition type | Suggested size |
| ----------- | --------- | -------------- | -------------- | 
| /mnt/boot or /mnt/efi | /dev/efi_system_partititon | EFI system partition | at least 260M |
| SWAP | /dev/swap_partition | Linux swap | more then 512M |
| /mnt | /dev/root_partition | Linux | Remainded of the device|


### Format partitions

### Mount the file systems

## Installation

### Select the mirrors

### Install essential packages

## Configure the system

### Fstab

### Chroot

### Time zone

### Localization

### Network configuration

### Initramfs (only for LVM, system encryption or RAID)

### Root password

### Boot loader

## Reboot

Exit chroot enviroment, unmount all partitions and reboot.


**Minimal Arch Linux is installed!!!**

## Post-installation

Create user and install packages.

- https://wiki.archlinux.org/index.php/General_recommendations